Source: kf6-kdnssd
Section: libs
Priority: optional
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Aurélien COUDERC <coucouf@debian.org>,
           Patrick Franz <deltaone@debian.org>,
Build-Depends: debhelper-compat (= 13),
               dh-sequence-kf6,
               dh-sequence-pkgkde-symbolshelper,
               cmake (>= 3.16~),
               doxygen,
               extra-cmake-modules (>= 6.11.0~),
               libavahi-common-dev,
               qt6-base-dev (>= 6.6.0~),
               qt6-tools-dev (>= 6.5.0~),
Standards-Version: 4.7.0
Homepage: https://invent.kde.org/frameworks/kdnssd
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/kf6-kdnssd
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/kf6-kdnssd.git
Rules-Requires-Root: no

Package: libkf6dnssd-data
Architecture: all
Depends: ${misc:Depends},
Multi-Arch: foreign
Description: Abstraction to system DNSSD features
 KDNSSD is a library for handling the DNS-based
 Service Discovery Protocol (DNS-SD), the layer of
 Zeroconf that allows network services.
 .
 This package contains the data files.

Package: libkf6dnssd-dev
Section: libdevel
Architecture: any
Depends: libkf6dnssd6 (= ${binary:Version}),
         qt6-base-dev (>= 6.6.0~),
         ${misc:Depends},
Recommends: libkf6dnssd-doc (= ${source:Version}),
Description: development files for kdnssd
 KDNSSD is a library for handling the DNS-based
 Service Discovery Protocol (DNS-SD), the layer of
 Zeroconf that allows network services.
 .
 Contains development files for kdnssd.

Package: libkf6dnssd-doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends},
Description: Abstraction to system DNSSD features (documentation)
 KDNSSD is a library for handling the DNS-based
 Service Discovery Protocol (DNS-SD), the layer of
 Zeroconf that allows network services.
 .
 This package contains the qch documentation files.
Section: doc

Package: libkf6dnssd6
Architecture: any
Multi-Arch: same
Depends: libkf6dnssd-data (= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
Description: Abstraction to system DNSSD features
 KDNSSD is a library for handling the DNS-based
 Service Discovery Protocol (DNS-SD), the layer of
 Zeroconf that allows network services.
